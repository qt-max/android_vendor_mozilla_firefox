#!/bin/sh

set -e

cd "$(dirname "$0")"

wget -O Fennec.apk 'https://f-droid.org/repo/org.mozilla.fennec_fdroid_1160020.apk'
rm -rf lib
unzip Fennec.apk 'lib/*'
