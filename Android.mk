LOCAL_PATH := $(call my-dir)
include $(CLEAR_VARS)
LOCAL_MODULE_TAGS := optional
LOCAL_MODULE := Fennec
LOCAL_SRC_FILES := Fennec.apk
LOCAL_MODULE_CLASS := APPS
LOCAL_MODULE_SUFFIX := $(COMMON_ANDROID_PACKAGE_SUFFIX)
LOCAL_CERTIFICATE := PRESIGNED
# Avoid rezipping the apk and damaging the v2/v3 signatures.
LOCAL_REPLACE_PREBUILT_APK_INSTALLED := $(LOCAL_PATH)/$(LOCAL_SRC_FILES)
# Compressed JNI libs must be extracted.
# The list of libs is acquired from the APK, but the paths refer to the
# libs in the filesystem, extracted by download-apk.sh.
LOCAL_PREBUILT_JNI_LIBS := $(shell zipinfo -1 "$(LOCAL_PATH)/$(LOCAL_SRC_FILES)" 'lib/*')
LOCAL_PRODUCT_MODULE := true
LOCAL_OPTIONAL_USES_LIBRARIES := androidx.window.extensions androidx.window.sidecar
LOCAL_OVERRIDES_PACKAGES := Jelly
include $(BUILD_PREBUILT)
